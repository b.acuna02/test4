package com.dci.taller4.services;

import com.dci.taller4.models.Automovil;
import com.dci.taller4.models.Proveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dci.taller4.repositories.AutomovilRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class AutomovilService {

    @Autowired
    private AutomovilRepository automovilRepository;
    @Autowired
    private ProveedorService proveedorService;

    public String createVehicle(Automovil automovil) {
        if(validarEmail(automovil) && validarRut(automovil.getRut()) && validarCodigoProducto(automovil)){
            Automovil automovil_a_crear = new Automovil();
            automovil_a_crear.setNombre(automovil.getNombre());
            automovil_a_crear.setEmail(automovil.getEmail());
            automovil_a_crear.setRut(automovil.getRut());
            automovil_a_crear.setStock(automovil.getStock());
            automovil_a_crear.setPrecio(automovil.getPrecio());
            automovilRepository.save(automovil);
        }

        return "ok";
    }

    public boolean validarRut(String rut) {
        List<Proveedor> proveedores = proveedorService.listProveedores();
        for (Proveedor proveedor : proveedores) {
            if (proveedor.getRut() != null && proveedor.getRut().equals(rut)) {
                return true;
            }
        }
        return false;
    }

    public boolean validarEmail(Automovil automovil) {
        List<Proveedor> proveedores = proveedorService.listProveedores();
        for (Proveedor proveedor : proveedores) {
            String proveedorEmail = proveedor.getNombre() + "@" + automovil.getNombre();
            if (proveedor != null && (automovil.getEmail().equals(proveedorEmail + ".cl") || automovil.getEmail().equals(proveedorEmail + ".com"))) {
                return true;
            }
        }
        return false;
    }

    public static boolean validarCodigoProducto(Automovil automovil) {
        String inicialDeMarca = automovil.getNombre().substring(0, 1).toUpperCase();
        String patronCodigo = generarCodigoFormateado(inicialDeMarca);
        System.out.println("Codigo patron: " +patronCodigo);
        return automovil.getCodigo().equals(patronCodigo);
    }

    private static String generarCodigoFormateado(String inicalDeMarca) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyHHmm");
        String patronTiempo = now.format(formatter);
        return inicalDeMarca + patronTiempo;
    }

    public Automovil parseInputString(String inputString) {
        String[] parts = inputString.split(",");
        Automovil automovil = new Automovil();
        automovil.setCodigo(parts[0]);
        automovil.setNombre(parts[1]);
        automovil.setStock(Integer.parseInt(parts[2]));
        automovil.setPrecio(Integer.parseInt(parts[3].replace(".", "").replace(",", "")));
        automovil.setRut(parts[4]);
        automovil.setEmail(parts[5]);
        return automovil;
    }
}





package com.dci.taller4.services;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
public class CSVLector {

    @Autowired
    private final ResourceLoader resourceLoader;

    public CSVLector(ResourceLoader resourceLoader){
        this.resourceLoader = resourceLoader;
    }

    public List<String[]> leerCSVProveedores() throws CsvException {
        Resource resource = resourceLoader.getResource("classpath:static/proveedores.csv");
        List<String[]> records = new ArrayList<>();
        try (Reader reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)) {
            CSVReader csvReader = new CSVReaderBuilder(reader)
                    .withCSVParser(new CSVParserBuilder().withSeparator(';').build())
                    .build();
            csvReader.skip(1);
            records = csvReader.readAll();
        } catch (IOException | CsvException e) {
            e.printStackTrace();
        }
        return records;
    }


}

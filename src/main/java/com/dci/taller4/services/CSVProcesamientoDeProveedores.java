package com.dci.taller4.services;
import com.dci.taller4.models.Proveedor;
import com.dci.taller4.repositories.ProveedorRepository;
import com.opencsv.exceptions.CsvException;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CSVProcesamientoDeProveedores {

    @Autowired
    CSVLector csvLector;

    @Autowired
    ProveedorRepository proveedorRepository;

    @PostConstruct
    public void inicializar() {
        try {
            processProveedoresCSV();
        } catch (CsvException e) {
            e.printStackTrace(); // Maneja la excepción según tus requisitos
        }
    }

    public void processProveedoresCSV() throws CsvException {
        List<String[]> proveedorData = csvLector.leerCSVProveedores();
        for (String[] data : proveedorData) {
            Proveedor proveedor = new Proveedor();
            System.out.println(data[0]);
            proveedor.setRut(data[0]);
            System.out.println(data[1]);
            proveedor.setNombre(data[1]);
            proveedorRepository.save(proveedor);
        }
        System.out.println("ok");
    }

}

package com.dci.taller4.services;

import com.dci.taller4.models.Proveedor;
import com.dci.taller4.repositories.ProveedorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProveedorService {

    @Autowired
    private ProveedorRepository proveedorRepository;

    public List<Proveedor> listProveedores(){
        return proveedorRepository.findAll();
    }
}

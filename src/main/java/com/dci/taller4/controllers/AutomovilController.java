package com.dci.taller4.controllers;

import com.dci.taller4.models.Automovil;
import com.dci.taller4.services.AutomovilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AutomovilController {

    @Autowired
    AutomovilService automovilService;

    @PostMapping("/createProduct")
    public String createVehicle(@RequestBody String inputString) {
        Automovil automovil = automovilService.parseInputString(inputString);
        if (automovilService.validarEmail(automovil)
                && automovilService.validarRut(automovil.getRut())
                && automovilService.validarCodigoProducto(automovil)) {
            automovilService.createVehicle(automovil);
            return "ok";
        } else {
            return "Invalid data. Please check email, rut, and product code.";
        }
    }

}

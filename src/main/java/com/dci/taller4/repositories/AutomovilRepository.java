package com.dci.taller4.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dci.taller4.models.Automovil;
import org.springframework.stereotype.Repository;

@Repository

public interface AutomovilRepository extends JpaRepository<Automovil,Integer>{

}

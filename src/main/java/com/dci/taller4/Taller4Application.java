package com.dci.taller4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.dci.taller4")
public class Taller4Application {

	public static void main(String[] args) {
		SpringApplication.run(Taller4Application.class, args);
	}

}
